---
title: "Showcase Tempo"
logo_url: /showcase/images/logo-tempo-white.png
heading1: "Becoming your own company"
byline: "How Tempo turned a consulting challenge into a business"
lede: "Tempo products are designed to enhance Jira by helping teams collaborate, plan, budget, track, and work smarter in one central software platform. More than 11,000 customers in over 100 countries trust Tempo to be successful. Their customers include small startups to large-scale distributed enterprises such as NASA, Amazon, PayPal, Toshiba, Disney, BMW, and Princeton University. Tempo is an award-winning, best-selling app vendor in the Atlassian Marketplace and has an established network of more than 100 partners worldwide."
---

Ten years ago, Pétur Ágústsson was working at an IT consulting firm in Iceland when he and his colleagues ran into a challenge. They were using Jira Software to manage projects, and a separate tool to log their hours for billing. Keeping track of which clients to bill for what work was tricky, so Pétur and his colleagues took advantage of Jira’s open platform to build an app that solved the problem. The app allowed the consultants to log their hours in the same tool they used to track work and also to generate billing reports for customers much more easily. “It was revolutionary for us,” Pétur says.

More than that, it provided them an opportunity. They soon licensed their app to another consulting company that was trying to solve the same problem, and a new business was born. A decade later, [Tempo Timesheets](https://marketplace.atlassian.com/plugins/is.origo.jira.tempo-plugin/server/overview) is one of the most popular apps for Jira in the Atlassian Marketplace. Pétur is Chief Customer Officer at Tempo, which also sells [Tempo Planner](https://marketplace.atlassian.com/plugins/com.tempoplugin.tempo-planner/server/overview) and [Tempo Budgets](https://marketplace.atlassian.com/plugins/com.greffon.folio/server/overview). It’s a story of how an innovative team saw an opportunity in Jira’s open, extensible architecture and turned it into a business.

## An ecosystem within an ecosystem

In 2015, Tempo spun off from that IT consulting firm to become its own company with close to 100 employees and 11,000 customers for its suite of apps, which help teams better manage their projects in Jira. A further 3,000 people download the tools each month to try them out. Tempo has opened up the APIs to its own apps, allowing other developers to extend the functionality. “We’ve built an ecosystem within an ecosystem,” Pétur says.

He sees several advantages in building apps for Atlassian. It’s a fast-growing market with thousands of customers, providing Tempo with lots of opportunity for growth. Tempo is profitable and its sales are growing quickly each year, he says. In addition, Atlassian’s APIs and documentation are easy to work with, so Tempo can try out new ideas quickly.

To be successful at building apps, he recommends listening closely to customers. “Don’t assume you know what they want,” Pétur says. “Talk to users and find out what they really need.” He recommends starting with a simple app and adding new features over time.

## Building apps in the cloud era

Tempo builds apps for both the cloud and server versions of Jira. Building for Jira Cloud is quicker than building for the server version, he says, and the cloud provides more opportunity for integration with other tools. In the cloud era, he notes, customers expect more integrations out of the box.

The SaaS technology market is evolving fast, which means that developers who build apps for SaaS products need to stay nimble and pay attention to changing customer needs. “It can be hard work, but you have a great opportunity,” Pétur says.

Tempo continues to innovate and develop new ideas. It recently launched mobile versions of Tempo Planner and Tempo Timesheets, as well as a Chrome extension for Tempo Timesheets. It has also built integrations for Slack and Hipchat.

Atlassian is easy to work with, Pétur says, and he likes the company’s down-to-earth attitude. He remembers when Atlassian invited him to join the program back in 2009. “It was one phone call. They said, ‘We’re a no-bullshit company, welcome to the program.’ It felt very relaxed.”

The Atlassian Marketplace now has [more than 3,000 apps](http://marketplace.atlassian.com) and has generated over $100 million in total sales in the last fiscal year alone. Whether it’s streamlining incident management, collaborating via ChatOps, or optimizing a DevOps process, there’s an app for almost every need. *Almost*&mdash;because customers always want new capabilities for Atlassian products and deeper integrations with other services and tools. Identify one of those needs, and you might find yourself building the next Tempo.

{{% linkblock href="../xpandit/" img="/img/chat-bubble.png" text="Xpand IT" section="server" align="right" %}}
{{% linkblock href="../wittified/" img="/img/clouds.png" text="Wittified" section="cloud" align="left" %}}
