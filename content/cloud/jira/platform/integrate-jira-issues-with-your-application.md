---
title: "Integrate Jira issues with your application"
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
guides: guides
date: "2017-09-11"
---

# Integrate Jira issues with your application

Many organizations around the world rely on Jira as a source of truth in 
their business processes. Jira Software has more than 37,000 customers and 
Jira Service Desk is used in over 20,000 organizations.

Atlassian offers Jira REST APIs that allow you to easily integrate with
Jira Cloud, Server, and Data Center. In addition to integrating
with our REST APIs, Jira also provides you with a framework that allows
you to customize Jira's UI with an app. You can reach new prospects by 
listing your app in the [Atlassian Marketplace](https://marketplace.atlassian.com) .

## Integrating on the Atlassian platform

The Atlassian platform provides different tools and solutions for integration
and extension:

* **Product integration**: Use the REST APIs and webhooks. 
  Authenticate using OAuth1 or Basic Authentication.
* **Atlassian Connect**: Use REST APIs and webhooks as well as surface 
  information inside Jira using UI extension points. This framework uses JSON Web Token authentication and provides in-app discovery & installation.

Head over to [Integrating with Jira Cloud](/cloud/jira/platform/integrating-with-jira-cloud/)
to learn more about the frameworks, features, and services that you can use. 

## Integration categories

There are three categories of integration that you can develop:

- **Enhance your app with Jira REST APIs:**
 - Create, update, or delete Jira issues from your application or service
 - Read data from Jira to display issues in your application
- **Enhance and extend Jira issues:**
 - Show info from another application on the Jira Issue page
 - Automatically update or transition issues from your app
- **Add your content to Jira projects:**
 - Add a page to the Jira project sidebar
 - Add custom reports to the Jira reports section

We're only going to look at examples from the first three points to gain
more of an understanding what's involved with building each. 

## Enhance your application with Jira REST APIs

### Create a Jira issue from your application or service

Integrating Jira into your service is a great way to make your service more robust.

Let's take a look at an example of this pattern we call a one-way integration currently in production.  A one-way integration is the most basic form of integration and the most simple to implement.

<img src="/cloud/jira/platform/images/sauce-labs-integration.png" alt="Sauce Labs">

SauceLabs creates a modal form right in their UI that allows users to capture bug information.

#### How to do it

The first thing you need to decide before writing a line of code is which authorization to Jira will you use.  You have two choices:

* OAuth Authentication (OAuth 1.0a)
* Basic Authentication

We recommend that you use OAuth over basic authentication, unless you are building tools like personal scripts or bots. With basic authentication, the username and password are sent repeatedly with requests and cached on the web browser, which is much less secure than OAuth (even if credentials are sent via SSL/TLS for basic HTTP). OAuth requires more work to implement, but it uses a token-based workflow which is much more secure.

#### Resources

* [OAuth for REST APIs](/cloud/jira/platform/jira-rest-api-oauth-authentication/)
* [Basic auth for REST APIs](/cloud/jira/platform/jira-rest-api-basic-authentication/)
* [Jira Cloud platform REST API](/cloud/jira/platform/rest/)
 
#### Jira REST APIs needed 

* [Create issue](/cloud/jira/platform/rest/#api-issue-post)
* [Get all projects](/cloud/jira/platform/rest/#api-project-get)
* [Get create issue meta](/cloud/jira/platform/rest/#api-issue-createmeta-get)


### Display related Jira issues in your application

Another way to enhance your application or service is to pull in relevant Jira issues into your user interface using Jira REST APIs.  Use JQL to pull in all issues from a specific project or tag issues with Entity Properties and pull just those issues back.  Use JQL to search for issues of a certain criteria and display those. The possibilities are endless. 

Entity properties allow apps to add key/value stores to Jira entities, such as issues or projects. These values can be indexed by Jira and queried via the REST API or through JQL.

<img src="/cloud/jira/platform/images/front-app-integration.png" alt="FrontApp">

FrontApp uses the Jira REST API to display existing issues into its UI. Users can then select a relevant issue to attach FrontApp information to the Jira issue.

#### How to do it

Again you'll need to decide how you will authenticate with Jira, being [basic](/cloud/jira/platform/jira-rest-api-basic-authentication/) or [OAuth](/cloud/jira/platform/jira-rest-api-oauth-authentication/) authentication. 

#### Resources

* [Entity properties](https://developer.atlassian.com/cloud/jira/platform/jira-entity-properties/)
* [JQL documentation](https://confluence.atlassian.com/jirasoftwarecloud/advanced-searching-764478330.html)
* [Jira Cloud REST APIs](/cloud/jira/platform/rest/)

#### Jira REST APIs needed

* [Get project](/cloud/jira/platform/rest/#api-project-projectIdOrKey-get)
* [Search with JQL](/cloud/jira/platform/rest/#api-search-get)
* [Set a property](/cloud/jira/platform/rest/#api-issue-issueIdOrKey-properties-propertyKey-put)

## Enhance and extend Jira issues

### Show info from another app on the Jira Issue page

Lastly, you can add value to your application or service by adding contextual information on a Jira page.  With an Atlassian Connect app you can extend predefined UI locations to any issue. 

An Atlassian Connect app is a web application that you host and that Jira admins can install into their instance of Jira Cloud.  Once they install your app they will have access to your application or service right in Jira.

There are many ways to extend Jira's UI but here we will look specifically at enhancing a Jira issue with web panels. 

<img src="/cloud/jira/platform/images/harvest-add-on.png" alt="Harvest">

Harvest extends Jira by adding in a time tracking widget into a predetermined UI extension point. 

#### Resources

* [Build a Jira app using a framework](/cloud/jira/platform/build-a-jira-app-using-a-framework/)
* [App descriptor](/cloud/jira/platform/app-descriptor/)
* [Web panel](/cloud/jira/platform/modules/web-panel/)

#### Code examples

* [My reminders](https://bitbucket.org/atlassianlabs/my-reminders/overview)
* [Who's looking](https://bitbucket.org/atlassian/whoslooking-connect/overview)

## Next steps

Hopefully, you've gained an idea of what's possible.  Integrating Jira issues with your application or your
integration with Jira makes your application that much more important to the ecosystem.

Need more help?  Head on over to the [Atlassian Developer Community](https://community.developer.atlassian.com)
and start a conversation with other developers in the Atlassian Ecosystem. 
