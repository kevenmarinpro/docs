---
title: "Change notice - removal of comments from issue webhooks in Jira Cloud"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-01-10"
---

# Change notice - removal of comments from issue webhooks in Jira Cloud

Following the [deprecation notice](/cloud/jira/platform/deprecation-notice-removal-of-comment-object-data-in-jira-issue-webhooks/), comment data will be removed from issue webhooks starting from the **22nd of January 2018**.  

The following webhooks will no longer contain any `comment` objects in their payload:

- `jira:issue_created`
- `jira:issue_deleted`
- `jira:issue_updated`

Note that, as a consequence, `jira:issue_updated` webhook will no longer be fired for comment-related events.

This change will be rolled out gradually, starting from **22 January 2018** and it may take up to two weeks until it reaches all Jira Cloud instances.


## What will happen if I do nothing?

If your app expects that `comment` objects are present in any of `jira:issue_*` webhooks, you are likely to encounter unexpected behaviour. Also, your app will stop receiving `jira:issue_updated` events when a comment is added, updated or removed from an issue.  

## Replacement

Since the deprecation notice published in October 2016, we recommended to use the `comment_created`, `comment_deleted` and `comment_updated` [webhooks](/cloud/jira/platform/webhooks/) as a replacement.

Comment webhooks have been recently enhanced with:

* [issue context provided in the comment webhook payload](https://ecosystem.atlassian.net/browse/ACJIRA-1152)
* [ability to apply JQL filter to webhook events](https://jira.atlassian.com/browse/JRACLOUD-68235)

We believe that with these improvements, `comment_*` webhooks successfully cover the majority of cases when apps relied on comment data in the `jira:issue_*` webhooks, and we highly recommend that you switch your apps to rely on `comment_*` webhooks before this change takes in effect.


If you found a bug, want to raise a feature request, or report an app security incident with [Jira Cloud APIs](developer.atlassian.com/cloud/jira/platform/rest) or [webhooks](/cloud/jira/platform/webhooks/) please open an issue over at the [Atlassian Ecosystem Developer Service Desk](http://go.atlassian.com/devhelp).
