---
title: "OAuth 2.0 authorization code grants (3LO) for apps"
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
- /jiracloud/security-overview.html
- /jiracloud/security-overview.md
date: "2018-06-15"
---
# OAuth 2.0 authorization code grants (3LO) for apps

{{% warning title="Draft status"%}}
This page is a work in progress. The content may change at any time without notice.<br>
Note, OAuth 2.0 authorization code grants is currently available for Jira Cloud platform, but not Jira Software or Jira Service Desk.
{{% /warning %}}

This page shows you how to configure your app to use _OAuth 2.0 authorization code grants_ (also known as "three-legged OAuth" or "3LO"). Note, you cannot use OAuth 2.0 authorization code grants with Atlassian Connect apps.

OAuth 2.0 authorization code grants are used to allow external applications and services to access Atlassian product APIs on a user's behalf. For example, if a user has approved access to a Zapier app, then that app can post a message in any of that user's Stride conversations.

#### Which authentication method should I use?

*   **[Connect apps](/cloud/jira/platform/getting-started/)**: Use [JWT](/cloud/jira/platform/authentication-for-apps/). You can also use [OAuth 2.0 user impersonation](/cloud/jira/platform/oauth-2-jwt-bearer-token-authorization-grant-type/). 
*   **[Other apps](/apps)**: Use OAuth 2.0 authorization code grants for authentication and authorization.
*   **Not using an app**: Use [OAuth 1.0a](/cloud/jira/platform/jira-rest-api-oauth-authentication/).

## Overview

OAuth 2.0 authorization code grants involve three parties: An Atlassian site (_resource_), a user (_resource owner_), and an external application/service (_client_). For example, a Jira site (_resource_), an Atlassian user (_resource owner_), and Gmail (_client_). Underlying the authorization interactions between these three parties is an authorization server.

To the user, the authorization process looks like this:

![authorization process for user](/cloud/jira/platform/images/3lo-process-for-user.png)

1.  The app directs the user to an Atlassian screen that prompts them to grant access to their data on the Atlassian site. The screen displays the access being requested in the Atlassian product.
1.  For example, the screenshot below shows the scopes in Jira that the app wants access to.
    ![3lo consent screen](/cloud/jira/platform/images/3lo-consent-screen-jira.png)
1.  The user grants (or denies) access to their data on the Atlassian site, via the screen.
1.  The user is directed back to the external service. If the user granted access, the external service can now access data (within the specified scopes) from the Atlassian site on the user's behalf. 

Underlying this process are a number of interactions between the external service, the app, and the authorization server. The full process is described in more detail below.

![authorization process for app](/cloud/jira/platform/images/3lo-process-for-app.png)

Note, this process assumes that the external service has registered an app with Atlassian that can use OAuth 2.0 authorization code grants.

1.  The user, in the external service, uses a feature that requires data from an Atlassian product.
1.  The external service seeks authorization to access the product's APIs, within the specified scopes, on behalf of the user.
1.  The user is directed to the Atlassian account login screen, if they are not already logged in, and prompted to log in.
1.  The user is directed to the authorization URL for the authorization server. This displays a screen that prompts the user to grant access to their data.
1.  If the user grants access, the user is directed to the callback URL with an authorization code.
1.  The app makes a POST to the token URL for the authorization server, exchanging the authorization code for an access token.
1.  The access token can now be used to access the APIs for the authorized Atlassian site on behalf of the user. This can be used until the token expires or is revoked.

Enabling OAuth 2.0 authorization code grants
--------------------------------------------

Before you can implement OAuth 2.0 authorization code grants for your app, you need to enable it for your app in app management:

1.  In [app management](/apps/), navigate to your app (or create one if you don't already have one).
1.  In the **Enabled APIs** tab, enable the **Jira API** if it is not already enabled.
1.  In the **Inbound authorization** tab, do the following:
  1.  Enable OAuth 2.0 authorization code grants (toggle the switch so that it shows a green tick).
  1.  Enter the **Callback URL**. You can set this to any URL that is accessible by the app. When you implement OAuth 2.0 authorization code grants in your app (see next section), the _redirect\_uri_ must match this URL. 

Your screen should look something like this:
![3lo app management](/cloud/jira/platform/images/3lo-app-management-jira.png)


Implementing OAuth 2.0 authorization code grants
------------------------------------------------

Once you have enabled OAuth 2.0 authorization code grants for your app, you can implement it in your app's code. There are a number of key parts to this:

1.   [Direct the user to the authorization URL to get an authorization code](#authcode)
1.   [Exchange the authorization code for an access token](#accesstoken)
1.   [Authorize any calls to the product APIs using the access token](#apicall)
1.   [Check site access for the app](#siteaccess) 

### 1. Direct the user to the authorization URL to get an authorization code ### {#authcode}

As described in the _Overview_ above, your app should start the authorization flow by directing the user to the authorization URL:

``` text
https://accounts.atlassian.com/authorize?
  audience=api.atlassian.com&
  client_id=YOUR_CLIENT_ID&
  scope=REQUESTED_SCOPE_ONE%20REQUESTED_SCOPE_TWO&
  redirect_uri=https://YOUR_APP_CALLBACK_URL&
  state=YOUR_USER_BOUND_VALUE&
  response_type=code&prompt=consent
```

Use this URL in a **GET** request. The query parameters are described below:

*   `client_id`: (_required_) Set this to the **Client Id** for your app. Find this in [app management](/apps/) under **Enabled APIs**. 
*   `scope`: (_required_) Set this to the scopes for the API (see [scopes section](#scopes) below). Separate scopes with a space. Note, you cannot specify scopes for more than one product.
*   `redirect_uri`: (_required_) Set this to the callback URL configured for your app in app management.
*   `state`: (_required for security_) Set this to a value that is associated with the user you are 
    directing to the authorization URL, e.g., a hash of the user's session ID. Make sure that this is a 
    value that cannot be guessed. You may be able to generate and validate this value automatically, if 
    you are using an OAuth 2.0 client library or an authentication library with OAuth 2.0 support. For 
    more information, including why this parameter is required for security, see [What is the state parameter used for?](#faq6) below.
*   `response_type`: (_required_) Set to `code` as you are requesting an authorization code (not a token).
*   `prompt`: (_required_) Set to `consent` so that the screen prompting the user to grant access will display.

If successful, the user will be redirected to the app's callback URL, with an authorization code provided as a query parameter called `code`. This code can be exchanged for an access token, as described in step 2.

#### Jira scopes for OAuth 2.0 authorization code grants #### {#scopes}

The Jira Cloud REST API supports the following scopes. These scopes are specific to OAuth 2.0 authorization code grants (i.e., different to Connect scopes). 

| Scope definition           | Scope name     | Description                                   |
| -------------------------- | ---------------|---------------------------------------------- |
| `read:jira-user`      | View user profiles | View user information in Jira that you have access to, including usernames, email addresses, and avatars. |
| `read:jira-work`      | View Jira issue data | Read Jira project and issue data, search for issues, and objects associated with issues like attachments and worklogs. |
| `write:jira-work`      | Create and manage issues | Create and edit issues in Jira, post comments as you, create worklogs, and delete issues |
| `manage:jira-project`      | Manage project settings | Create and edit project settings and create new project-level objects (e.g. versions and components) |
| `manage:jira-configuration`      | Manage Jira global settings | Take Jira administration actions (e.g. create projects and custom fields, view workflows, manage issue link types) |

Be aware that an app will always be constrained by the permissions of the user that an app is acting for, regardless of its scopes. For example, if an app has the _manage:jira-configuration_ scope but user does not have the _Administer Jira_ permission, then the app will not be able to take Jira administration actions.

The following table shows which scopes is required for an operation. If an operation is not listed, then you cannot use it with OAuth 2.0 authorization code grants:

**API**|**Method**|**Scope**
-----|-----|-----
/attachment|GET|`read:jira-work`
/attachment|DELETE|`write:jira-work`
/component|GET|`read:jira-work`
/component|POST|`manage:jira-project`
/component|PUT|`manage:jira-project`
/dashboard/properties|GET|`read:jira-work`
/field|GET|`manage:jira-configuration`
/field|POST|`manage:jira-configuration`
/field|PUT|`manage:jira-configuration`
/field|DELETE|`manage:jira-configuration`
/filter|GET|`read:jira-work`
/issue|GET|`read:jira-work`
/issue|POST|`write:jira-work`
/issue|PUT|`write:jira-work`
/issue|DELETE|`write:jira-work`
/issue/properties|GET|`read:jira-work`
/issue/properties|PUT|`write:jira-work`
/issue/properties|DELETE|`write:jira-work`
/issueLink|GET|`read:jira-work`
/issueLink|POST|`write:jira-work`
/issueLink|DELETE|`write:jira-work`
/issueLinkType|GET|`read:jira-work`
/issueLinkType|POST|`manage:jira-configuration`
/issuetype|GET|`read:jira-work`
/issuetype|POST|`manage:jira-configuration`
/issuetype|PUT|`manage:jira-configuration`
/issuetype|DELETE|`manage:jira-configuration`
/mypermissions|GET|`read:jira-user`
/myself|GET|`read:jira-user`
/priority|GET|`read:jira-work`
/project|GET|`read:jira-work`
/project|POST|`manage:jira-configuration`
/project|PUT|`manage:jira-project`
/project/properties|GET|`read:jira-work`
/project/properties|PUT|`manage:jira-project`
/project/properties|DELETE|`manage:jira-project`
/search|GET|`read:jira-work`
/search|POST|`read:jira-work`
/status|GET|`read:jira-work`
/user|GET|`read:jira-user`
/version|GET|`read:jira-work`
/version|POST|`manage:jira-project`
/version|PUT|`manage:jira-project`
/version|DELETE|`manage:jira-project`
/workflow|GET|`manage:jira-configuration`
/worklog|GET|`read:jira-work`
/worklog|POST|`write:jira-work`

### 2. Exchange authorization code for access token ### {#accesstoken}

``` bash
curl --request POST \
  --url 'https://accounts.atlassian.com/oauth/token' \
  --header 'Content-Type: application/json' \
  --data '{"grant_type": "authorization_code","client_id": "YOUR_CLIENT_ID","client_secret": "YOUR_CLIENT_SECRET","code": "YOUR_AUTHORIZATION_CODE","redirect_uri": "https://YOUR_APP_CALLBACK_URL"}'
```

*   `client_id`: (_required_) Set this to the **Client Id** for the Stride API for your app. Find this in app management under **Enabled APIs**.
*   `client_secret`: (_required_) Set this to the **Client Secret** for the Stride API for your app. Find this in app management under **Enabled APIs**.
*   `code`: (_required_) Set this to the authorization code received from the initial authorize call (described above).
*   `redirect_uri`: (_required_) Set this to the callback URL configured for your app in app management.

If successful, this call will return an access token. This access token can be used to make API calls, as described below.

### 3. Make calls to the API using the access token ### {#apicall}

Your app now has an access token that it can use to authorize requests to the APIs for the Atlassian site. When making a call to an API, pass the access token as a bearer token in the header of the request. This will authorize the request on the user's behalf.

``` bash
curl --request GET \
  --url https://api.atlassian.com/ex/jira/{cloudid}/{api} \
  --header 'Authorization: Bearer ACCESS_TOKEN' \
  --header 'Accept: application/json'
```

Note that `api.atlassian.com/ex/jira/{cloudid}/{api}` not `https://your-domain.atlassian.net/rest/api/2/` is used for OAuth 2.0 authorization code grants. If you are copying the examples in the [Jira REST API documentation](https://developer.atlassian.com/cloud/jira/platform/rest), you will need to amend the example URLs as they currently show `https://your-domain.atlassian.net/rest/api/2/` only.

### 4. Check site access for the app ### {#siteaccess}

An authorization grant is when a user consents to your app accessing a specific site and APIs within that site (via scopes). This can change when either of the following occur:

* The user revokes the grant for the site.
* The user consents to a new grant for the site. The scopes in the new grant override the scopes in the existing grant.

Therefore, since a grant can change over time, it's important that you check your app's access to a site and its APIs when calling the site's APIs. You can do this by calling the `accessible-resources` endpoint (_Get list of resources_) on `https://api.atlassian.com`. This endpoint is described below:

#### Get list of resources

`GET /oauth/token/accessible-resources`

##### Request

Request parameters: None

Example:

``` bash
curl --header 'Authorization: Bearer <access_token>' \
  --url 'https://accounts.atlassian.com/oauth/token/accessible-resources'
```

##### Response

`200 OK` Schema:

``` json
"AccessibleResourcesResponse": {
  "items": {
    "$ref": "#/definitions/AccessibleResources"
  },
  "type": "array"
},
"AccessibleResources": {
  "properties": {
    "avatarUrl": {
      "type": "string"
    },
    "id": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "scopes": {
      "items": {
        "type": "string"
      },
      "type": "array"
    }
  },
  "required": [
    "id",
    "name",
    "scopes"
  ],
  "type": "object"
}
```

Each item in the response describes a container (e.g., a Jira site) that your app has access to, the scopes associated with that access, and metadata such as the name and avatar URL (if any). It's important to understand that this endpoint won't tell you anything about the user's permissions, which may limit the resources that your app can access via the site's APIs.

Note, the `id` is not unique across containers (i.e., two entries in the results can have the same `id`), so you may need to infer the type of container from its scopes.

**Congratulations!** Your app can now use OAuth 2.0 authorization code grants to access an Atlassian product's APIs on behalf of a user.

## Frequently asked questions

*   [How do I get a new access token, if my access token expires or is revoked?](#faq1)
*   [What happens if a user grants access to more than one Atlassian site for an app?](#faq2)
*   [What is the state parameter used for?](#faq3)
*   [Why can't my app reference entity properties in JQL?](#faq4)

### How do I get a new access token, if my access token expires or is revoked? ### {#faq1}

You have two options:

*   Initiate the entire authorization flow from the beginning again.
*   Use a refresh token to get another access token.

A refresh token can be returned with the access token in your initial authorization flow. To do this, add the `offline_access` scope to the **scope** parameter of the authorization URL. 

Once you have the refresh token, exchange it for an access token by calling the token URL:

``` bash
curl --request POST \
  --url 'https://api.atlassian.com/oauth/token' \
  --header 'Content-Type: application/json' \
  --data '{ "grant_type": "refresh_token", "client_id": "YOUR_CLIENT_ID", "client_secret": "YOUR_CLIENT_SECRET", "refresh_token": "YOUR_REFRESH_TOKEN" }'
```

* `grant_type`: Set to `refresh_token`.
* `client_id`: Set this to the **Client Id** for the Stride API for your app. Find this in app management under **Enabled APIs**.
* `client_secret`: Set this to the **Client Secret** for the Stride API for your app. Find this in app management under **Enabled APIs**.
* `refresh_token`: The refresh token that you obtained with your original access token.

If successful, a new access token will be returned that you can use to make calls to the product API.

### What happens if a user grants access to more than one Atlassian site for an app? ### {#faq2}

Only one grant exists per app for a given Atlassian account. If a user grants access to more than one Atlassian site for this app, then the additional sites are added to the same grant. This means that existing access tokens will give you access to all sites and scopes that a user has granted your app access to.

### What is the state parameter used for? ### {#faq3}

The primary use for the state parameter is to associate a user with an authorization flow. This makes 
the authorization flow more secure, as the authorization flow cannot be hijacked to associate a user's 
account with another user's token. Consider the following example scenario using [Stride](https://www.stride.com/):

1. An application, named _Reminders\_Application_, has a Stride integration that implements OAuth 2.0 
   authorization code grants but does not specify a `state` parameter.
2. A malicious actor, Mallory, initiates a Stride authorization flow for herself. This could be via the 
   _Reminders\_Application_ or by crafting an authorization URL that includes the _Reminders\_Application_'s `client_id`.
3. Mallory blocks the request to the _Reminders\_Application_'s callback URL during the authorization flow. 
   She records the URL, including the `code` parameter.
4. Mallory tricks another user, Edward, into visiting the callback URL in his browser.
5. The _Reminders\_Application_ handles the callback and exchanges Mallory's code for an access token to Stride. 
   Edward is logged into the _Reminders\_Application_ and the callback request came from Edward's browser, so 
   Mallory's token is now linked to Edward's account.
6. Mallory now has access to information sent to Edward by the _Reminders\_Application_ via the Stride 
   integration. For example, the _Reminders\_Application_ may send a reminder about a confidential meeting to 
   Edward in Stride, however the reminder would be sent to Mallory instead.
   
If the _Reminders\_Application_ integration had used a `state` parameter, the _Reminders\_Application_ would have known that the
callback URL belonged to Mallory and ignored the request.

Other uses for the `state` parameter include:

- Acting as a key for keeping track of specific details about the flow.
- Returning the user to the right step in their workflow after sending them through the authorization flow.

### Why can't my app reference entity properties in JQL? ### {#faq4}

Apps can store and read the values of entity properties (issue properties and project properties) using the REST API. However, in the current implementation of OAuth 2.0 authorization code grants, apps cannot declare [searchable entity properties](/cloud/jira/platform/jira-entity-properties/#making-searchable-entity-properties). This means that if your app uses OAuth 2.0 authorization code grants, it won't be able to refer to entity properties in JQL queries.

