---
title: "OAuth 2.0 - JWT bearer token authorization grant type"
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2017-08-25"
---
{{< include path="docs/content/cloud/connect/concepts/oauth2-jwt-bearer-token-authentication.snippet.md">}}